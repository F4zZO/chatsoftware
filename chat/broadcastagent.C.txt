#include <stdio.h>
#include <pthread.h>
#include <mqueue.h>
#include <semaphore.h>
#include "broadcastagent.h"
#include "network.h"
#include "user.h"
#include "util.h"

#define MQ_NAME "/msg_queue"
#define MQ_MODE ((int)(0644))
#define MQ_MAXMSG 10
#define MQ_MSGSIZE 1024
#define MQ_FLAGS 0
#define MQ_CURMSGS 0

static mqd_t messageQueue;
static pthread_t threadId;
static int status;
static int lock = 0;
static sem_t sem;
static int msq;

static void *broadcastAgent(void *arg)
{
	for(;;)
	{
		Message msg_;
		Message *msg = &msg_;
		status = mq_receive(messageQueue,(char*) msg, MQ_MSGSIZE, NULL);
		if(status < 0)
		{
			errorPrint("Message reading failed");
		}
		sem_wait(&sem);
		msq--;
		userForEach(msg, networkSend);
		sem_post(&sem);
	}
	return arg;
}

int broadcastAgentInit(void)
{
	if(sem_init(&sem, 0, 1U) != 0)
	{
		errnoPrint("Failed to initialize semaphore");
		return -1;
	}
	struct mq_attr attr;
	attr.mq_flags = MQ_FLAGS;
	attr.mq_maxmsg = MQ_MAXMSG;
	attr.mq_msgsize = MQ_MSGSIZE;
	attr.mq_curmsgs = 0;
	messageQueue = mq_open(MQ_NAME, O_CREAT | O_RDWR, MQ_MODE, &attr);
	mq_unlink(MQ_NAME);

	if(messageQueue < 0)
	{
		errorPrint("Failed to create MessageQueue");
		return -1;
	}
	debugPrint("MessageQueue created");

	msq = 0;
	status = pthread_create(&threadId, NULL, broadcastAgent, NULL);
	if(status < 0)
	{
		errorPrint("Failed to create thread");
		return -1;
	}
	debugPrint("Created thread");
	return 0;
}

void broadcastAgentCleanup(void)
{
	pthread_exit(&threadId);

	sem_destroy(&sem);

	debugPrint("Cleaning up MessageQueue");
	mq_close(messageQueue);
}

int broadcastAgentAddMq(const Message *buffer, int prio)
{
	if(msq < 10)
	{
		if(mq_send(messageQueue,(char *) buffer, MQ_MSGSIZE, prio) < 0)
		{
			errorPrint("Failed to send to MessageQueue");
			return -1;
		}
		debugPrint("Message sent to MessageQueue");
		msq++;
		return 0;
	}
	return -1;
}

int broadcastUnlockSem(void)
{
	if(lock == 1)
	{
		sem_post(&sem);
		lock = 0;
		debugPrint("Broadcast Semaphore unlocked successfully");
		return 0;
	}
	errorPrint("Broadcast Semaphore already unlocked");
	return -1;
}

int broadcastLockSem(void)
{
	if(lock == 0)
        {
                sem_wait(&sem);
		lock = 1;
                debugPrint("Broadcast Semaphore locked successfully");
		return 0;
        }
	errorPrint("Broadcast Semaphore already locked");
	return -1;
}

int broadcastCheckSem(void)
{
	if(lock == 1)
	{
		return 0;
	}
	return -1;
}
